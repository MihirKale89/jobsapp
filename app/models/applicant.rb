class Applicant < User
  has_many :mappings
  has_many :employers, through: :mappings
end
