class Employer < User
  has_many :mappings
  has_many :applicants, through: :mappings
end
