class CreateMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :mappings do |t|
      t.integer :applicant_id
      t.integer :employer_id

      t.timestamps
    end
    add_index :mappings, :applicant_id
    add_index :mappings, :employer_id
    add_index :mappings, [:applicant_id, :employer_id], unique: true
  end
end
